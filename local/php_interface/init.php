<?php

class tovary {
     public static function deaktivirovat() {
         $select = Array ();
         $filter = Array ('ACTIVE' => 'Y', '=AVAILABLE' => 'N');
         $res = \CIBlockElement::GetList(Array(), $filter, false, false, $select);
         
         while($ob = $res->GetNextElement()){ 
             $fields = $ob->GetFields();
             
             $el = new CIBlockElement;
             $el->Update($fields['ID'], ['ACTIVE' => 'N']);
         }
     }
}

AddEventHandler("iblock", "OnIBlockElementUpdate", Array ('tovary', 'deaktivirovat'));